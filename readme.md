# xfel-mode-line

This is a simple but effective way of using echo area as mode-line.
It is implemented as a minor mode, so you could use it as a global one or disable it whenever you don't want it anymore.
